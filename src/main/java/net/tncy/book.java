package net.tncy;

public class book
{
    private String name;
    private String auteur;
    private int isbn;

    public book(String name, String auteur, int isbn) {
        this.name = name;
        this.auteur = auteur;
        this.isbn = isbn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }
}